Nomenclature
============

To establish a concise and unambiguous language, this chapter contains
the nomenclature used within the context of the KauriID project.


Involved Parties
----------------

The parties explained will also be used directly as PROV *Agents* used
in a KauriID context.

A subject *
    The identity owner (e.g. the user themself making a statement of
    an ID attribute, usually the "expressor").
    (Alternative term suggestion: principal).

An attester
    An asserter, attesting the validity of a user's ID attribute
    (e.g. an organisation in possession of proof of an identity
    attribute, such as a utilities bill for an address).

A requestor
    A party/company/organisation wanting access to a user's ID
    attribute.

A claim issuer
    An actor issuing a claim. This can be either the subject
    themselves, or an attester issuing the claim (usually within the
    same process of making an attestation, for simplicity reasons).

\* Term to be re-addressed later if a more suitable term is to be
found.


ID Entities
-----------

The entities explained will also be used directly as PROV *Entities*
used in a KauriID context.

A claim
    An attribute claim originating from the subject (user, principal).
    Claims can be issued by the subject ("self claim") or by some
    other party ("foreign claim", e.g. by the attester).

A claim set
    A set of claimed identity attributes made with a single claim
    statement (and to be attested in a single attestation). A claim
    set is not to be confused with a basket (that possibly contains
    attributes referenced from different claim sets).

An attestation
    Relates to a claim, and has a time to live (TTL, expiry date).
    Note: Some attributes may have additional expiry dates
    (e.g. validity period of a driver licence) that is to be
    extracted/stored/managed/validated as well. The expiry of the
    attestation is independent of this.

A property or attribute
    Contains the actual content of a particular piece of identity
    information. These terms can be used interchangeably (for now).

A proof
    A piece of 'evidence' in support of a claim.  Is not required to
    be stored in IPFS, but *must* be referenced uniquely in terms of
    what it is. To achieve this, a a (more-or-less) entity naming
    scheme is required at some point of time (to compose well-formed
    URIs for this). Some evidence may be stored in a representation
    (e.g. a scan of a driver licence) on IPFS.

A schema
    A collection of attributes related to a subject, e.g. a 'person'
    from schema.org.  Schemas can be nested.

A basket *
    A collection of (schema) attributes. Baskets are shareable as a
    whole (as opposed to individual attributes only).  A basket can
    contain elements across schemas (thus not mandatorily following a
    nested schema pattern). It might be sensible to restrain from
    nesting baskets to keep things "flat".  (Alternative term
    suggestions: bundle, bouquet).  Baskets can be "incomplete" (if
    required attributes for a given schema are still missing),
    "complete" (if all schema relevant attributes are given) or
    "augmented" (if additional attributes are contained).

A revocation *
    To be addressed later. An invalidation of an attestation in some
    form.  This may require a 'revocation list' (which is *not*
    something that could 'taint' directly a user's
    claims/attestations).

An attestation statement *
    An attestation statement contains meta-data about the particular
    attestation expressed. An attestation may contain one or more
    statements, expressing information on governance and/or
    compliance, as suitable for the use case. Each of the statements
    is associated with a time to live (expiry), thus enabling an
    attester to express a validity period for a particular purpose,
    but possibly giving the attestation an additional (extended) life
    period for 'lesser' purposes.

An PROV entity can be:

- A proof,
- a basket,
- a claim,
- or an attestation.

\* Term to be re-addressed later if a more suitable term is to be
found.


An Application (App)
--------------------

An embodyment of a requesting or attesting application, thus
effectively a requestor or attester plus some logic.


..
    Local Variables:
    mode: rst
    ispell-local-dictionary: "en_GB-ise"
    mode: flyspell
    End:
