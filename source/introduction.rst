Introduction
============

Kiwis can't express their identity digitally and securely across
cultural backgrounds, across competitive boundaries. This is an
ongoing, permanent problem so far. Yes, there are ways for it,
e.g. using RealMe, Google federated identity, etc. But they all have
their "warts". Some are expensive or cumbersome to use from an
organisation's perspective. Others are leaking meta-data to
corporates, whose goal is to use your information to be able to sell
to you in a better way, thus making the end user The Product (TM). Yet
others are lacking critical mass among the population to be
successful.

A Bloomberg Intelligence's Report "[The_Year_Ahead_2018]_" is quoting
the cost for the US banking sector for KYC (Know Your Customer) or AML
(Anty Money Laundering) breaches to total US$ 16.1 billion from 2008
to 2015. The same report cites the Royal Bank of Scotland to employ
2,000 staff (early 2017) exclusively to comply with KYC rules, with
the expectation to lower this headcount by 95% given a viable digital
solution. Due to the magnitude of this problem, a local major bank has
kicked off an initiative with the local community to venture into
solution opportunities.

This paper presents background to the problem statement, the goal
definition and particularly the approach taken for the system. Design
decisions and evaluations will be discussed for this system under the
project title of "Kauri ID", a self-sovereign, blockchain-based
identity infrastructure. It puts the user at the centre, and no
company or organisation owns identity information or acts as a
(formal) guardian. Kauri ID employs privacy by design, enabling
fine-granular, selective and confidential data sharing. Authenticity
is implemented via a web of trust, attesting identity attribute
claims. Even though Kauri ID is inherently self-sovereign, sovereign
aspects can be catered for via governmental attribute endorsements,
thus building a bridge between New Zealand's RealMe system and
Kauri ID.


Overview
--------

Kauri ID is an industry led group who are exploring and shaping a New
Zealand self-sovereign digital identity system leveraging
blockchain/distributed ledger technology. The goal is to "Reimagine
the future of digital identity for Kiwis and New Zealand."  The reason
is, that at the moment it is **very difficult** for Kiwis to express
their identity digitally and securely. However, the solution sought is
likely to be transferable across borders and applicable elsewhere.


Problems/Challenges
^^^^^^^^^^^^^^^^^^^

Kiwis do not have the ability to express their identity digitally and
securely across cultural backgrounds and competitive boundaries --
This is a core challenge being faced in New Zealand and there is a
growing need for a Digital Identity solution.  Currently, New
Zealanders are required to prove their identity multiple times when
signing up to a new service, they have to go through difficult
procedures and cannot easily verify their identity in a frictionless
way at a time and place that suits them. This results in a lot of
effort for the customer and an inconsistent digital ID verification
process across the industry.

For companies, the compliance costs to on board new customers and
maintain accuracy of customer identity information are increasingly
becoming more expensive, particularly for those who have to ensure the
accuracy of their customer data and/or go through complex AML/KYC
procedures. Companies are increasing their security and data
protection costs to ensure the safety of customer information. There
are multiple companies who are trying to find solutions for these
problems ranging from government agencies such as Real ME, to global
platforms such as Google and Facebook. However they all have their
challenges or pitfalls from a customer and business perspective.  Some
are expensive or cumbersome to use from an organisation's
perspective. Others are leaking meta-data to corporates, whose goal is
to use your information to be able to sell to you in a better way,
thus making the end user *The Product* (TM). Yet others are lacking
critical mass among the population to be successful.

For consumers, as the use of digital channels and connectivity between
entities increase, there are a number of identity-dependent
transactions that are motivating users to demand more control over who
has access to their data. When the data changes in real life (such as
a change of address, or a change in a company's ownership structure),
the customer is obliged to tell the various service providers they
have relationships with.  Proofs of identity are usually unstructured
data, taking the form of images and photocopies.  Some forms of proof
(e.g. photocopies of original documents) can be easily faked, meaning
extra steps to prove authenticity need to be taken, such as having
photocopies notarised, leading to extra friction and expense. This
results in expensive, time-consuming and troublesome processes,
increasing user frustration.

As stated in a World Economic Forum paper on Digital Identity,
regulators are also demanding increased transparency around
transactions and institutions are required to provide greater
granularity and accuracy in the identity information they capture
[WEF]_. These challenges can restrict and put limitations on new,
innovative offerings companies are able can bring out to market.

These challenges are only likely to increase as the need for a number
digitally identity-based transactions increase and the risk of data
breaches and identity theft attempts become more sophisticated and
frequent. For example, the recent Equifax data breach in 2017,
affected more than 143 million US consumers [BBC]_. Their social
security numbers, which previously were a core aspect of a user's
identity, are now able to be used by other parties. Equifax stated
that it expects the costs of related to the data breach to surge by
$275 million in 2018 suggesting the incident could turn out to be the
most costly hack in corporate history [Reuters]_. A Bloomberg
Intelligence's Report ("[The_Year_Ahead_2018]_") is quoting the cost
for the US banking sector for KYC (Know Your Customer) or AML
(Anti-Money Laundering) breaches to total US$ 16.1 billion from 2008
to 2015. The same report cites the Royal Bank of Scotland to employ
2,000 staff (early 2017) exclusively to comply with KYC rules, with
the expectation to lower this headcount by 95% given a viable digital
solution.

This leads to the core problem identified by the Kauri ID group --
Kiwis do not have the ability to express their identity digitally and
securely across cultural backgrounds and competitive boundaries.

.. pull-quote::

   Kiwis can't express their identity digitally and securely across
   cultural backgrounds and competitive boundaries.


What is Kauri ID?
^^^^^^^^^^^^^^^^^

Due to the magnitude of this problem, a local major bank kicked off an
initiative with the companies in the local community in September 2017
to venture into solution opportunities. This has involved
non-competing companies who share the same principles and goals to
create an altruistic solution that will benefit all of New
Zealand. Kauri ID have started creating a self-sovereign identity
platform built on blockchain/distributed technology that puts the user
at the centre, and no company or organisation owns identity
information or acts as a (formal) guardian.

Self-sovereign identity is the concept that people and businesses can
store their own identity data on their own devices, and provide it
efficiently to those who need to validate it, without relying on a
central repository of identity data. It's a digital way of doing what
we do today with bits of paper.  This digital concept is very similar
to the way we keep our non-digital identities today. Today, we keep
passports, birth certificates, utility bills at home under our own
control, maybe in an "important drawer", and we share them when
needed. We don't store these bits of paper with a third
party. Self-sovereign identity is the digital equivalent of what we do
with bits of paper now.

Kauri ID employs privacy by design, enabling fine-granular, selective
and confidential data sharing. Authenticity is implemented via a web
of trust, attesting identity attribute claims. Even though Kauri ID is
inherently self-sovereign, sovereign aspects can be catered for via
governmental attribute endorsements, thus building a bridge between
New Zealand's Real ME system and Kauri ID.

Kauri ID attempts to respond to identifying "real" customers in an
increasingly digital world where non-digital solutions are no longer
accepted as the normal, while still complying with regulatory
requirements.


Benefits of a Digital Identity Solution
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Need to know, but not more
    Authorities could provide "bundles" of attested claims, such as
    "over 18", "over 21", "accredited investor", "can drive cars"
    etc., for the user to use as they see fit. The identity owner
    would be able to choose which piece of information to pass to any
    requester. For example, if you need to prove you are over 18, you
    don't need to share your date of birth, you just need a statement
    saying you are over 18, signed by the relevant authority.

    Sharing this kind of data is safer both for the identity provider
    and the recipient. The provider doesn't need to overshare, and the
    recipient doesn't need to store unnecessarily sensitive data --
    for example, if the recipient gets hacked, they are only storing
    "Over 18" flags, not dates of birth. Even banks themselves could
    attest to the person having an account with them. We would first
    need to understand what liability they take on when they create
    these attestations. We would assume it would be no more than the
    liability they currently take on when they send you a bank
    statement, which you use as a proof of address elsewhere.

Affordability
    Cheap to adopt for organisations as well as end-users.

Ease of use
    Easy to use for both organisations and end-users.

Secure
    Avoid problems with current employment of passwords for
    authentication through easy use of cryptographic means.

Privacy
    Strong enforcement of privacy aspects to prevent leakage of
    content as well as meta-data

User-centric
    Self-sovereignty and everything related to one's identity is
    ultimately at a user's decision whether to add it to their
    identity or reveal it to a requestor.

Cross-domain
    Used by: Private sector, public sector, large and small
    organisations/companies, strongly regulated or not regulated,
    financial organisations, government, other organisations.

Open/Public
    No proprietary IP, flexible in use, can be rolled out elsewhere,
    policies/governance can be 'wrapped around' the technical bits to
    suit (hopefully) every environment/usage scenario.


Elevator Pitch
--------------

The "Elevator Pitch" is the short description of the idea, explaining
the concept in a way such that any listener can understand it in a
short period of time.

    For every individual who has to provide personal information,
    Kauri ID is a mechanism that securely provides access to selected
    personal data using Blockchain technology.  Unlike RealMe,
    Facebook & Google, the individual is in control of their own
    information and who has access to what.



..
   Take input from ThinkSmash:

   "Reimagine the future of digital identity for Kiwis and New Zealand"

   It is **very difficult** for Kiwis to express their identity digitally
   and securely.

   **TRUST!!!**


   Take input from here:
   https://cloudblogs.microsoft.com/enterprisemobility/2018/02/12/decentralized-digital-identities-and-blockchain-the-future-as-we-see-it/

      Each of us needs a digital identity we own, one which securely and
      privately stores all elements of our digital identity.  This
      self-owned identity must be easy to use and give us complete
      control over how our identity data is accessed and used.


..
    Local Variables:
    mode: rst
    ispell-local-dictionary: "en_GB-ise"
    mode: flyspell
    End:
