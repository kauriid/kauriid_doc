Data Structures and Encryption
==============================

The underlying data structures for Kauri ID are extremely demanding in
a security and authenticity context. For this reason, this chapter
outlines the general approach and nesting of core data structures in
the overall design.

All data encryption or signing is to be performed using the JSON
Object Signing and Encryption (JOSE, RFC 7520) standards.


Requirements
------------

(A) The ability to make "self claims" by the subject (to be attested
    by another party).
(B) The ability to make "foreign claims" (usually by the attester,
    usually attested immediately).
(C) The ability to keep content of claims/attributes confidential.
(D) The ability to decrypt every claim element/attribute independently
    (separate encryption key).
(E) The ability to reference each claim element/attribute
    independently (tuple: claim set reference, attribute index).
(F) The ability to verify the authenticity of every claim
    element/attribute independently.
(G) Uphold opaqueness of the type of attribute (or named key) to an
    outside observer.
(H) Uphold opaqueness of the attester (signer) of a claim to an
    outside observer.
(I) A requester by default is only enabled to retrieve the attestation
    meta-data of a single attestation.
(J) A requester can be enabled to recursively retrieve the attestation
    meta-data of the entire (backward facing) attestation provenance
    trail.


Considerations
--------------

- Make sure that a "leaking" of a record (not direct subject -> requestor passing) will be detectable (put recipient into the passed data)
- Use a "commitment" approach to signing:
    - hash individually salted records
    - sign the hashes including own public key as "commitment"
    - make sure that the the subject stores the salts together with the individual keys
    - The commitment is part of the attestation
    - The attestation (containing an "attester commitment") thus *must* be counter-signed by the subject/owner (via their own "subject commitment") upon receipt to validate it for use

- Attestation meta-data:
    - various types of meta-data for an attestation
    - information on evidence provided (direct documents, scans, copies, ...)
    - information on attestation method (physical, digital, ...)
    - information on the validity period (time to live)
    - free information on regulatory concerns or governance
    - provenance trail information of the attestation, parties and entities involved

      
to share a basket:

- referenced attestation with the claim set
- claim set key objects with selective key information only (on individual claims, trace keys, etc.)


Implementation details for commitment:

- claims are converted to a compact JSON representation in UTF-8 encoding
- salts for individual claim commitments are 16 bytes
- each claim commitment element is ``SHA-256(salt || UTF-8 claim JSON)``
- the committer's public signing key is contained
- the commitment subject (ID owner) is contained
- the committer "role" is contained ("attester" or "subject")
- each commitment element is URL-safe base64 encoded (stripped off padding)
- commitment elements, salts and the public signing key (all in stripped URL-safe base64 encoding) are joined in order, separated by a '``.``' character in the following sequence:
  ``salt_0, commitment\_element_0, salt_1, commitment\_element_1, ..., salt_{n-1}, commitment\_element_{n-1}``
- all commitment data is expressed as a JSON object in compact UTF-8 encoding
- this JSON object is signed using JOSE JWS (using ``{"alg": "Ed25519"}``)


Recommendations
---------------

- The order (permutation) of claims within a claim set should be
  randomised/shuffled (to more strongly protect the (G) requirement)
  to avoid any inference in the claim attribute's reference to its
  type (e.g. first element is likely to be street name/number).

- Use of distinct symmetric keys for each individual ``ClaimSet``
  object as well as for each individual ``Claim`` within. External
  ``Attestation`` objects (thus not embedded within the ``ClaimSet``'s
  meta-data) are to be encrypted with the ``ClaimSet``'s object key.

- Use of a separate ``trace_key`` to enable the recursive retrieval of
  attestation meta-data.
  
- Use of the NaCl ("Sodium" library) cipher suite for all cryptography
  purposes within the JOSE context:

    - Use of JSON Web Encryption with **ChaCha20/Poly1305** for
      symmetric key encryption of content (JWE, RFC 7516, RFC 7539,
      RFC 8439)
    - Use of JSON Web Encryption with ECDH-ES using
      **X25519/ChaCha20/Poly1305** for public key encryption to another
      party (JWE, RFC 7516, RFC 8037, RFC 7539, RFC 8439)
    - Use of JSON Web Signing for cryptographic signatures via EdDSA
      using **Ed25519** (JWS, RFC 7515, RFC 8037)
    - Use of JSON Web Keys for storage/management of cryptograhpic
      keys (JWK, RFC 7517, RFC 7638)


Data Structures
---------------

.. image:: _static/data_structures.*

.. todo::

   @Guy Think about recovery of subject's master key in case of loss
   (e.g. via redundancy coding, nifty multi-key crypto schem, ...)


..
    Local Variables:
    mode: rst
    ispell-local-dictionary: "en_GB-ise"
    mode: flyspell
    End:
