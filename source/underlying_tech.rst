Underlying Technology
=====================

This chapter contains information on a number of underyling technology
choices for the implementation and operation of KauriID. Ideally,
components can be interchanged for others with little or no
alterations to other components. This will be improbable to achieve
fully, however the aim is to keep developing the system in a decoupled
fashion to assist evolution in the future. A core focus was taken on
selecting components for openness (e.g. open source licence and
community).


Blockchain
----------

The system is to be based on the Ethereum blockchain. It is possible
that it will (initially) operate on a private or local instance to be
feasible with regards of scalability and costs. However, this should
make it much easier to transition over time towards the common public
Ethereum chain as scalability issues are being addressed. This
transition is desirable for reasons of immutability and trust.


Core Identity Platform
----------------------

For reasons of maturity and a good overlap with the desired features
we are using the [uPort]_ self-sovereign identity and user-centric
data platform. It is made available under the liberal Apache 2.0 open
source licence, with an open source implementation of further
libraries and tools on the road map.

The uPort project provides a host of tools, helping in accelerating
and securing the development:

- White-label client SDK (future, announced)
- Web client and connection libraries
- A relay service

.. todo::

   @Paul to flesh out some details on uPort (to contrast it from
   alternative options)


Data Storage
------------

Data storage on the (Ethereum) blockchain is expensive and bloats the
distributed ledger. Therefore it is desirable to minimise the amount
of on-chain storage. For this reason, we are relying on off-chain
storage using [IPFS]_, with only 32 byte addresses (cryptographic
hashes) to store as references per data item using uPort on Ethereum.

IPFS provides a distributed, peer-to-peer, immutable storage
system. Immutability is achieved via addressing content through the
application of a cryptographic hash function, thus assuring that
content is consistent and cannot be tampered with.


Web Client Libraries
--------------------

The Ethereum JavaScript API (``web3.js``) is used for access to
Ethereum. The uPort project provides further libraries for direct
uPort-specific access.


Attestation Provenance
----------------------

All processes and relationships of KauriID-based records need to be
expressed in some way. For reasons of standardisation and flexibility,
the [PROV]_ standard as outlined by the World Wide Web Consortium
(W3C) was chosen. It meets the needs for all attestations, and is
highly integratable with other systems, other/external storage
systems, other recording perspectives or degrees of granularity as
well as with embedded, linked or externalised PROV storage. It is
particularly suitable to consistently express audit trails as they are
for example useful in anti money laundering (AML) workflows. As the
data formats are machine readable, they can potentially also be
automatically audited. PROV provides a mature standard, that has
evolved considerably through various provenance projects since the
early 2000s into its present state.

Provenance trails can be stored in "pieces" on various storage
systems. These trails can be represented in a number of serialisation
formats (that can be converted into each other) according to needs,
allowing for extensive interoperability.



..
    Local Variables:
    mode: rst
    ispell-local-dictionary: "en_GB-ise"
    mode: flyspell
    End:
