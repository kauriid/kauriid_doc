A User Journey
==============

To highlight the design of KauriID, we have set aside an exemplary
user journey, outlining a number of fictitious (but exemplary)
steps. Each step touches a different aspect of KauriID's general
conceptual design.

1. **Onboarding for AA Membership, making a virgin KauriID**
   (with DL and POA, extracting attributes from DL and POA, including photo)

2. **Local Library Membership**
   (with attested POA "basket" from step 1)

3. **Purchase alcohol**
   (with attested DOB and picture attribute only from step 1)

4. **Onboarding for a Loan at ASB**
   (with Passport and S&P Agreement)

   - ASB attests a stronger identity and updates POA "basket"
   - Updated POA "basket" can be shared back to AA and Local Library
     (user's choice)

5. **Connecting power from Mercury**
   (online only, using latest from step 4)

6. **Registering to vote in local election with KauriID records**
   (with latest from step 4)


Detailed Description
--------------------

Step 1: Onboarding for AA Membership
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The user shows up in an AA centre with the driver licence and a
Watercare bill for proof of address. The AA representative offers him
to at the same time help create a KauriID identity.

The user downloads the KauriID app and creates a new (empty)
KauriID. The AA representative then helps to populate it with the
identity attributes required for a new AA membership. AA will go about
verifying the details as the usually do (using physical artifacts),
and make an attestation of a number of extracted details from these on
the user's new KauriID.

The following figure outlines the attestation as expressed through
PROV assertions. The PROV *Entities* (yellow ovals) are linked in a
directed graph with PROV *Activities* (blue rectangles) and PROV
*Agents* (orange house shapes). The linking directed edges are
expressing the PROV usage, generation and derivation
relationships. Information on the attributes contained in the entities
are provided in the note boxes. The example uses a utilities bill (by
Watercare) with a driver licence (issued by NZTA) into a new
attestation. An AA staff member Bob is acting on behalf of the
organisation AA.

.. image:: _static/kauriid-prov-aa.*


Step 2: Local Library Membership
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The "basket" containing some key identity attributes (attested) is
accessed by the library. The attributes contain:

- ``name``
- ``dob``
- ``street``
- ``suburb``
- ``city``
- ``post_code``


Step 3: Purchase Alcohol
^^^^^^^^^^^^^^^^^^^^^^^^

The identity attributes (attested) accessed by the shop are limited to:

- ``dob``
- ``photo``


Step 4: Onboarding for a Loan at ASB
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The user enters an ASB branch to apply for a loan on their new
house. As the KauriID attestations are not "strong enough", further
identity validation is required. For this, the user brought in a
passport and the sales and purchase agreement for the property.

*Update of Identity Attestations:* With this, ASB is able to make a
stronger identity attribute attestation, including their "proof of
address" attribute basket.

This next figure extends the previous attestation entity (from step 1,
in a grey oval), by first strengthening the identity attestation
through the use of a passport presented to the bank teller "Ray" in
the organisation "ASB". In a second step the address is being updated
by means of a sales and purchase agreement from a real estate agency.

.. image:: _static/kauriid-prov-asb.*

With the availability of these new attestations for an address, the
user may chose to (selectively) **propagate this information** through
to those previous requestors. The liquor shop from step 3 is probably
not intended to be informed. The local library and AA (from steps 1
and 2) however may be likely, depending on the location of the new
property.


Step 5: Connecting Power from Mercury
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The user visits Mercury's web site to order the new house being
connected to power. For a first time customer, Mercury is satisfied
with the level of identity verification through the bank's
attestation, even though the transaction is solely conducted
online. No further documents (for identity or proof of address) are
required to be scanned and submitted via the web site or email.

The attributes shared with Mercury would be similar to those shared
with the local library (step 2), maybe with the addition of attributes
such as a phone number.


Step 6: Registering to Vote
^^^^^^^^^^^^^^^^^^^^^^^^^^^

The user wants to register to vote. For this the industry-based
identity attestations are not sufficient. Thus, a visit to the local
NZ Post branch is required to acquire a RealMe attestation on the
KauriID identity accepted by DIA.

In this last PROV trace, the existing KauriID user records (grey oval)
is augmented via a RealMe attestation. In this the chain of delegation
through various organisations is highlighted.

.. image:: _static/kauriid-prov-realme.*


..
    Local Variables:
    mode: rst
    ispell-local-dictionary: "en_GB-ise"
    mode: flyspell
    End:
