Example Operations
==================

The [PyKauriID]_ library can be used to exercise the example
operations. In the following shell commands for the CLI client are
provided.

Let us assume that Harry Potter will want to have his home address and
identity details attested by his head master Albus Dumbledore.

For this example, a number of cryptographic keys need to be
present. These are expressed in individual files adhering to the JSON
JWK standard. (All files are present in the ``examples/`` directory of
the [PyKauriID]_ project's Git repository).

- ``harry_sig_full.jwk`` - Harry's 'full' signing key (includes the
  private key).
- ``harry_sig_pub.jwk`` - Harry's public signing key only.
- ``dumbledore_sig_full.jwk`` - Dumbledore's 'full' signing key
  (includes the private key).
- ``dumbledore_sig_pub.jwk`` - Dumbledore's public signing key only.


Creating/Accessing a Claim Set
------------------------------

The first step is to create a claim set for Harry. Afterwards all data
within the claim set can be accessed with the help of the generated
claim set keys file.


Create a Claim Set
^^^^^^^^^^^^^^^^^^

Harry's full JSON-LD compliant identity record, containing nine
individual claims:

.. code:: json

   {
       "@context": "http://schema.org",
       "@type": "Person",
       "familyName": "Potter",
       "givenName": "Harry James",
       "birthDate": "1980-07-31",
       "address": {
           "@type": "PostalAddress",
           "streetAddress": "4 Privet Drive",
           "addressLocality": "Little Whinging",
           "postalCode": "BS13 9RN",
           "addressRegion": "Surrey",
           "addressCountry": "United Kingdom",
           "telephone": "+44 555 325 1464"
       }
   }

Each claim separately then can be extracted into a single JSON-LD
claim object. For example for his given name:

.. code:: json

   {
       "@context": "http://schema.org",
       "@type": "Person",
       "givenName": "Harry James"
   }

Or for the nested street address field:

.. code:: json

   {
       "@context": "http://schema.org",
       "@type": "Person",
       "address": {
           "@type": "PostalAddress",
           "streetAddress": "4 Privet Drive"
       }
   }

For the purpose of this example, these are collated in a JSON array
within the file ``harry_claims.json``.

The ``kauriid.py`` CLI tool now will create a new claim set (file
``_claim_set_harry.jwe``, JWE encrypted) from these claims. The claim
set will contain the cryptographically signed "commitment" of the
owner, and all encryption keys will be stored in a claim set keys file
(``_claim_set_keys_harry.json``).

.. code:: sh

   kauriid.py claims --operation new \
       --sig_key harry_sig_full.jwk \
       --claims harry_claims.json \
       --claimset _claim_set_harry.jwe \
       --claimsetkeys _claim_set_keys_harry.json


Access a Claim Set
^^^^^^^^^^^^^^^^^^

Any element within this claim set can now be accessed (decrypted,
verified and displayed) again when providing the corresponding claim
set keys object. The following accesses the claim element with index
``2`` (indexing starts from ``0``):

.. code:: sh

   kauriid.py claims --operation access \
       --sig_key harry_sig_pub.jwk \
       --claimset _claim_set_harry.jwe \
       --claimsetkeys _claim_set_keys_harry.json \
       --index 2

To do the same for all elements, providing an index of ``-1``:

.. code:: sh

   kauriid.py claims --operation access \
       --sig_key harry_sig_pub.jwk \
       --claimset _claim_set_harry.jwe \
       --claimsetkeys _claim_set_keys_harry.json \
       --index -1


Making/Accessing an Attestation for a Claim Set
-----------------------------------------------

A claim set in itself is not worth much, because *anybody* could claim
to be Harry Potter. To give the identity attributes contained within
the claim set any weight, they need to be attested by a (mutually
trusted) third party. In this case, the headmaster Professor
Dumbledore will do this on behalf of Hogwarts, School of Witchcraft
and Wizardry.

For the CLI, the attester information is to be written to a JSON file
(``attester_data.json``). This information is particularly relevant
for picking up structure for the provenance/audit trail, formalising
relationships and references to be linked with the rest of the world.

.. code:: json

   {
       "provenance_namespaces": {
           "hogwarts": "http://www.hogwarts.ac.uk/ns/prov#"
       },
       "attester": "hogwarts:staff/2omXXXbobXXX...WEG",
       "delegation_chain": ["hogwarts:Organisation"]
   }

There is further data required for this specific attestation, captured
in another JSON file (``attestation_data.json``). It contains the
following information:

- *Provenance namespaces* provide basic information to simplify the
  audit trail, to be later used in the specific URIs referencing
  elements (e.g. the evidence elements).
- *Evidence elements* contain the references to documents provided to
  the attester for verification of the legitimacy of the attestation
  request.
- *Evidence verification* provides a reference to an entity (a
  document) describing the specific evidence verification procedure
  used in this case.
- *Ancestors* are references to attestations that are preceding this
  specific attestation. Through these a linked audit trail spanning
  previous (and future) attestations will be created. The ancestors
  each have an object specific encryption key (``object_key``) as well
  as a trace key, which allows to recursively access all previous
  elements of the trail.
- *Attestation statements* provide information on the validity and
  compliance of the attestation. One or more statements can be
  provided, each with a different expiry time (``ttl``, expressed in
  UNIX Epoch seconds) and free-form meta-data. The purpose for this is
  to be able to make an attestation towards a particular regulatory
  compliance, but allow for additional longer lasting attestations
  that may not require to be held up to this long period. For example
  a bank may have stringent requirements to only allow an attestation
  for KYC to be used for six months, however for general purposes the
  attestation may still be 'good enough'. In the example the first
  statement contains a ``ttl_span`` (in this case 90 days = 7,776,000
  seconds), which is used to *compute* the actual expiry time. The
  second statement contains an explicit time stamp (16 September 2019)
  until which the statement remains valid (e.g. the expiry of a
  document, another regulatory constraint, etc.).

.. code:: json

   {
       "provenance_namespaces": {
           "nzta": "http://www.nzta.govt.nz/",
           "watercare": "http://www.watercare.co.nz/"
       },
       "evidence_elements": {
           "nzta:dl/DJ034005-v193": "original document",
           "watercare:customers/4711/invoices/201809.pdf": "digital scan"
       },
       "evidence_verification": "kauriid:procedures/evidence_checks/ipns/QmdXXXcurrentmanualXXX...YgV",
       "ancestors": [
           {"uri": "kauriid:attestations/ipns/QmbXXXattestation01XXX...PTU",
            "object_key": {
                "kty": "oct", "alg": "dir", "enc": "ChaCha20/Poly1305",
                "use": "enc", "k": "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACR"},
            "trace_key": {
                "kty": "oct", "alg": "dir", "enc": "ChaCha20/Poly1305",
                "use": "enc", "k": "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"}
           }
       ],
       "statements": [
           {"ttl_span": 7776000,
            "metadata": {"kyc_compliance": "type 3",
                         "governance": "NZ, non-banking"}},
           {"ttl": 1568622642,
            "metadata": {"kyc_compliance": null,
                         "governance": "NZ, non-banking"}}
       ]
   }

Note: Guess what, it's an odd world, in which Harry is presenting a NZ
drivers' licence and serviced by NZ Watercare. Similarly NZ KYC
governance standards are applied. But ... in the magical world related
to a Kiwi project ... many things are possible.


Create an Attestation
^^^^^^^^^^^^^^^^^^^^^

The previously create claim set (and claim set keys) together with the
attester and attestation information are used to create a new
attestation in a file (``_attestation_harry.jwe``). A new trace key
for the attestation is generated at the time, which is added to the
claim set keys file.

.. code:: sh

   kauriid.py attestations --operation attest \
       --attestation _attestation_harry.jwe \
       --attester_data attester_data.json \
       --attestation_data attestation_data.json \
       --sig_key dumbledore_sig_full.jwk \
       --claimsetkeys _claim_set_keys_harry.json

The freshly created attestation now needs to be accepted
(counter-signed) by the identity owner.

.. code:: sh

   kauriid.py attestations --operation accept \
       --attestation _attestation_harry.jwe \
       --sig_key harry_sig_full.jwk \
       --claimsetkeys _claim_set_keys_harry.json


Access an Attestation
^^^^^^^^^^^^^^^^^^^^^

In the same way as before, elements (now attested claims) can be
individually accessed.

.. code:: sh

   # Access element 2 of the attestation.
   kauriid.py attestations --operation access \
       --attestation _attestation_harry.jwe \
       --claimsetkeys _claim_set_keys_harry.json \
       --index 2

   # Access all elements of the attestation.
   kauriid.py attestations --operation access \
       --attestation _attestation_harry.jwe \
       --claimsetkeys _claim_set_keys_harry.json \
       --index -1

In case the CLI (like here) has created the initial claim set, the
claim set keys file contains the types of the individual claims. These
can also be listed as follows:

.. code:: sh

   kauriid.py attestations --operation access \
       --attestation _attestation_harry.jwe \
       --claimsetkeys _claim_set_keys_harry.json \
       --list

All the information stored within an attestation (including all
meta-data, provenance/audit trail, evidence, ancestors, claims,
attestation statements, etc.) can be accessed and dumped in clear text to the terminal using the ``--dump`` option.

.. code:: sh

   kauriid.py attestations --operation access \
       --attestation _attestation_harry.jwe \
       --claimsetkeys _claim_set_keys_harry.json \
       --dump


..
    Local Variables:
    mode: rst
    ispell-local-dictionary: "en_GB-ise"
    mode: flyspell
    End:
