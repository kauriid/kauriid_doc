=============================
KauriID Project Documentation
=============================

This documentation consists of explanatory content on KauriID: A
blockchain-based, self-sovereign identity system, initiated through
the Wynyard Innovation Neighbourhood (WIN).

.. only:: latex

    .. raw:: latex

        \newpage

    .. include:: copyright.rst


.. todolist::

.. toctree::
   :maxdepth: 2
   :numbered:

   introduction
   nomenclature
   user_journey
   underlying_tech
   data_structures
   example_operations
   policies
   recovery
   references


..
    Local Variables:
    mode: rst
    ispell-local-dictionary: "en_GB-ise"
    mode: flyspell
    End:
