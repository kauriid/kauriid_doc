Policies
========

- Order in which attestations are added to the PROV trail
    - considering TTL (e.g. duration of expiry, not past expired ones?
- Where to continue on the attestation trail?
    - at the tip or earlier (see above on TTL, etc.)


..
    Local Variables:
    mode: rst
    ispell-local-dictionary: "en_GB-ise"
    mode: flyspell
    End:
