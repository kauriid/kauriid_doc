Recovery
========

Loss of credentials is a common issue in real world applications. It
would be foolish to believe that Kauri ID is exempt from
this. Therefore, we must provide means to enable users to recover
their identity even in cases of facing the loss of their identity
information or access to their credentials. These situations may occur
through loss or theft of a device, corruption or defect of the storage
media, forgotten access credentials, etc.

Due to the choice of the underlying technology stack, the uPort system
offers a number of methods to recover their identity. Due to the fact
that the identity is decoupled from access to the identity, it is the
*access* to the identity which is catered for in unforeseen
threatening circumstances.

The Kauri ID eco-system though is comprised also of additional
information (e.g. claims and attestations) that is related to, but
otherwise living outside the uPort system. This Kauri ID data storage
is recovered via a different mechanism, though trying to align itself
with the principles of the uPort recovery mechanisms.


uPort
-----

The uPort system (through its mobile app) offers three methods to
transfer an account:

- *Device Transfer* allows a user to migrate their uPort identities to
  a new device, usually under non-threatening circumtances such as a
  phone upgrade.
- *Social Recovery* allows a user to recover their uPort identities to
  a new device, by relying on a network of trusted contacts for
  validation. These contacts can be friends or trusted institutions.
  This mode is usually reserved for potentially threatening
  circumstances such as a lost or stolen phone.
- *Seed Recovery* allows a user to recover their uPort identities to a
  new device using a 12 word mnemonic.


Kauri ID Data Storage
---------------------

Kauri ID data beyond the uPort data is persisted using IPFS and a
claims registry service on the Ethereum blockchain. For the recovery
of such data two essential things need to be given: A mechanism for
discovery of all data elements stored for an identity, as well as
cryptographic keys to access an identity's own stored content.

For the purpose, the minimal required of information for re-discovery
is encoded in a single, secret data item. This secret is then divided
into a suitable number of fragments using Shamir's Secret Sharing
[SSS]_ algorithm. The algorithm splits this secret information into
:math:`n` different parts ("fragments"), with the property that the
original data can be reconstructed using a limited number of :math:`0
< k \leq n` fragments. Depending on the choice of :math:`k` and
:math:`n`, the security concerns of a user can be scaled according to
requirements.

In analogy to uPort's "social recovery" method, one can encrypt one of
the secret's portion to one of the :math:`n` trusted contacts, with a
:math:`k` selected according to the desired threshold or quorum
parameter. The portion of the secret is encrypted against the
recipient's public encryption key, thus allowing *only* the trusted
contact to access the portion.

Similarly, a user's master key (used for encrypting and discovering a
user's own data storage structures) can be transferred to a new
device, or accessed for secure backup purposes through a word mnemonic.


..
    Local Variables:
    mode: rst
    ispell-local-dictionary: "en_GB-ise"
    mode: flyspell
    End:
