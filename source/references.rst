.. only:: not latex

    ==========
    References
    ==========

.. [The_Year_Ahead_2018]
  | Source: https://www.bloomberg.com/professional/book-year-ahead-2018/
  | Reprint: https://bravenewcoin.com/news/blockchain-is-coming-everywhere-ready-or-not/

.. [WEF]
  | World Economic Forum, "A Blueprint for Digital identity"
  | Aug. 2016
  | http://www3.weforum.org/docs/WEF_A_Blueprint_for_Digital_Identity.pdf

.. [BBC]
  | BBC, "Massive Equifax data breach hits 143 million"
  | Sept. 2017
  | http://www.bbc.com/news/business-41192163

.. [Reuters]
  | Reuters, "Equifax breach could be the most costly in corporate history"
  | Sept. 2017
  | https://www.reuters.com/article/us-equifax-cyber/equifax-breach-could-be-most-costly-in-corporate-history-idUSKCN1GE257

.. [PROV]
  | PROV Overview: https://www.w3.org/TR/prov-overview/
  | PROV Primer: http://www.w3.org/TR/2013/NOTE-prov-primer-20130430/

.. [uPort]
  | Whitepaper: https://whitepaper.uport.me/
  | Project page: https://uport.me/

.. [IPFS]
  | Whitepaper: https://github.com/ipfs/papers/raw/master/ipfs-cap2pfs/ipfs-p2p-file-system.pdf
  | Project page: https://ipfs.io/

.. [PyKauriID]
  | PyKauriID - A Kauri ID reference library in Python
  | Project page: https://gitlab.com/kauriid/pykauriid
.. [SSS]
  | Adi Shamir, "How to share a secret"
  | Communications of the ACM, Volume 22 Issue 11, Nov. 1979, Pages 612-613
  | ACM New York, NY, USA 
  | https://doi.org/10.1145%2F359168.359176





..
    Local Variables:
    mode: rst
    ispell-local-dictionary: "en_GB-ise"
    mode: flyspell
    End:
