KauriID Project Documentation
=============================

version number: 0.0.1

A blockchain-based, self-sovereign identity system, initiated through
the Wynyard Innovation Neighbourhood (WIN).

Building
--------

### Install Requirements

The documentation is written using the Sphinx documentation tool.  You
may install them (in a virtual environment) using the requirements
listed in `requirements.txt`.

Set up and activate for Python 3:

    # If you don't have a directory for virtual environments, yet.
    mkdir -p ${HOME}/.virtualenvs/
    cd ${HOME}/.virtualenvs/

    # Create and activate the virtual environment.
    virtualenv --system-site-packages --python=/usr/bin/python3 \
               ${HOME}/.virtualenvs/kauriid_doc
    source ${HOME}/.virtualenvs/kauriid_doc/bin/activate

    # Install the required packages.
    pip install -r requirements.txt


### Build Documentation

To build, you may run `make html` or `make latex` in a shell (Unix) or
command prompt (Windows) in the root directory to generate
it. Alternatively any of the targets listed in `make help`. `make.bat`
in the same directory may also work under Windows systems (given all
requirements are met).
