#!/usr/bin/env bash

# Build the docs.
make clean
make html

# Commit and push.
git add -A
git commit -m "building and pushing docs"
git push origin master

# Switch branches and pull the data we want.
git checkout gh-pages
rm -rf .
touch .nojekyll
git checkout master build/html
mv ./build/html/* ./
rm -rf ./docs
git add -A
git commit -m "publishing updated docs..."
git push origin gh-pages

# Switch back.
git checkout master
